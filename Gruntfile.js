var path = require('path');


module.exports = function ( grunt ) {

	 /** 
	 * Load required Grunt tasks. These are installed based on the versions listed
	 * in `package.json` when you do `npm install` in this directory.
	 */

	 require('load-grunt-tasks')(grunt);

	var userConfig = {
		build_dir: "build",
		src_dir: "src",
		bower_dir: 'bower_components',
		bower_files: [
			"bootstrap/dist/js/bootstrap.min.js",
			"jquery/dist/jquery.min.js",
			"modernizr/modernizr.js"
		],
		layouts: [
			"home/home.ejs"
		]
	};

	var taskConfig = {


		/**
		* Local web server
		**/
		connect: {
			develop: {
				options: {
					hostname: '*',
					port: 3333,
					base: '<%= build_dir %>',
					livereload: true
				}
			}
		},

		/**
		*     Concatenate application javascript files into app.js
		**/
		concat: {
			js_build: {
				options: {
					banner: '(function ( window, undefined ) { \n',
					footer: '\n})( window );'
				},
				src: [
					'<%= build_dir %>/js/**/*.js',
				],  
				dest: '<%= build_dir %>/js/app.js'
			}
		},

		copy: {
			// Copy bower components during build task
			bower_components: {
				files: [
					{
						src: '<%= bower_files %>',
						dest: '<%= build_dir %>/js/',
						cwd: '<%= bower_dir %>/',
						expand: true,
						flatten: true
					}
				]
			},
			assets: {
				files: [
					{ 
									src: [ '**' ],
									dest: '<%= build_dir %>/img/',
									cwd: '<%= src_dir %>/img/',
									expand: true
					},
					{ 
									src: [ '**' ],
									dest: '<%= build_dir %>/fonts/',
									cwd: '<%= src_dir %>/fonts/',
									expand: true
					},
					 ]
			}
		},


		ejs: {
			all: {
				src: userConfig.layouts,
				dest: '<%= build_dir %>/',
				expand: true,
				flatten: true,
				cwd: '<%= src_dir %>/app',
				ext: '.html',
				rename: function rename(dest, matchedSrcPath, options) {
	              matchedSrcPath = matchedSrcPath.replace('home', 'index');
	              return path.join(dest, matchedSrcPath);
	            }
			},
		},

		less: {
			compile: {
				options: {
					cleancss: true,
					compress: true,
					paths: [
						'<%= bower_dir %>'
					],
				},
				files: {
					'<%= build_dir %>/css/main.css': '<%= src_dir %>/app/main/main.less'
				}
			},
			bootstrap: {
				options: {
					cleancss: true,
					compress: true,
					paths: [
						'<%= bower_dir %>'
					],
				},
				files: {
					'<%= build_dir %>/css/bootstrap.css': '<%= src_dir %>/app/main/bootstrap/bootstrap.less'
				}
			},
			fontAwesome: {
				files: {
					'<%= build_dir %>/css/font-awesome.css': '<%= src_dir %>/app/main/font-awesome/font-awesome.less'
				}
			}
		},

		/**
		 * JSHint our JS files
		 */
		jshint: {
			src: [ 
				'<%= src_dir %>/js/**/*.js',
				'!<%= src_dir %>/vendor/',
			],
			gruntfile: [
				'Gruntfile.js'
			],
			options: {
				smarttabs: true,
				curly: true,
				immed: true,
				newcap: true,
				noarg: true,
				sub: true,
				boss: true,
				eqnull: true
			},
		},

		

		delta: {

			options: {
				livereload: true
			},

			/**
			 * When the LESS files change, we need to compile and copy to build dir
			 */
			less: {
				files: [ '<%= src_dir %>/app/**/*.less' ],
				tasks: [ 'less:compile'],
			},

			/**
			 * When .ejs file changes, we need to compile ejs into HTML.
			 */
			html: {
				files: [ '<%= src_dir %>/app/**/*.ejs' ],
				tasks: [ 'ejs:all'],
			},

			assets: {
				files: [ 
					'<%= src_dir %>/img/**/*',
					'<%= src_dir %>/fonts/**/*',
					'<%= src_dir %>/js/**/*',
				],
				tasks: [ 'copy:assets' ]
			},

			js: {
				files: ['<%= src_dir %>/js**/*'],
			}

		}
	};

	grunt.initConfig( grunt.util._.extend( taskConfig, userConfig ) );
	// grunt.config.init(taskConfig);

	grunt.registerTask('build', [
		'jshint',
		'concat',
		'less',
		'ejs',
		'copy'
	]);

	grunt.renameTask( 'watch', 'delta' );
		grunt.registerTask( 'watch', [ 
			'build',
			'connect:develop',
			'delta' 
		]);



	grunt.registerTask('default', ['less:compile']);

};
